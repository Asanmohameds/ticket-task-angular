export interface Iticket
    {
        EnquiryId: Number;
        CreatedDatetime: Number;
        CreatedBy: Number;
        ContractNumber: Number;
        CustomerId: Number;
        CustomerName: string;
        StatusName: string;
        TicketTypeId: Number;
        PriorityName: string;
        Subject: string  
    }

export interface IcontractNo {
    ContractNumber: number;
}

export interface IemployeeId {
    EmployeeId: Number;
}

export interface IfourfieldsType {
    ID: number;
    Name: string;
}

export interface IcategoryType {
    TicketGroupID: number;
    TicketGroupName: string;
}

export interface IsubCategoryType {
    Id: number;
    Description: string;
}