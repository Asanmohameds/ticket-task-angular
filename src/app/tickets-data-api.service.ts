import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TicketsDataApiService {

  constructor(private http:HttpClient) { }

// Getting ERP Initial URL...

  urlErp() {

    return this.http.get<any>('https://erp.arco.sa:65//api/GetMyTicket?CustomerId=CIN0000150');
  }


  // Getting Contract List ERP...
  urlContract() {
    return this.http.get<any>("https://erp.arco.sa:65/api/GetTicketCustomerDetails?CustomerId=CIN0000150");
  }

  urlEmployee(contractNo:any) {

    let urlLabourId = "https://erp.arco.sa:65//api/GetTicketIndContractAllEmployee?CustomerId=CIN0000150&ContractId=";
    var urlLabourfilter = urlLabourId + contractNo;
    return this.http.get<any>(urlLabourfilter);
  }

  urlTicketType() {
    return this.http.get<any>("https://erp.arco.sa:65//api/TickettypeList");
  }

  urlDepartmentId(ticketId:any) {

    //console.log(ticketId);
    let urlDepartment = "https://erp.arco.sa:65//api/GetTicketAssignedToGroupByTicketTypeId?TicketTypeID=";
    let urlDepartmentfilter = urlDepartment + ticketId;
    return this.http.get<any>(urlDepartmentfilter);
  }

  urlAssignedTo(){
    return this.http.get<any>("https://erp.arco.sa:65//api/assigntoList");
  }

  urlPriorityList() {
    return this.http.get<any>("https://erp.arco.sa:65//api/PriorityList");
  }

  urlCategoryList(priorityId:any) {
    let urlCategory = "https://erp.arco.sa:65//api/GetTicketGroupByDepatmentId?TicketAssignGroupId=";
    let urlCategoryfilter = urlCategory + priorityId;
    return this.http.get<any>(urlCategoryfilter);
  }

  urlSubcategoryList(categoryId:any) {
    let urlSubcategory = "https://erp.arco.sa:65/api/SubGroupByGroup?id=";
    let urlCategoryAccumulated = urlSubcategory + categoryId;
    return this.http.get<any>(urlCategoryAccumulated);
  }

  //**************           POST DATA TO API      ************ */

  postData(params:any) {
    let urlApi = "https://erp.arco.sa:65//api/CreateTicketNew?UpdateTicketFields=";

    let paramsJson = JSON.stringify(params);

    let urlAccumulated = urlApi + paramsJson;

    return   this.http.post<any>(urlAccumulated, null);

  }

}
